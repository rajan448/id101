var funcList = []; 

for (var i = 0; i <2; i++) {

	var f =createFunction(i);
	funcList.push(f)
	
	function createFunction(x)
	{
		// local of variable "i" is created as "x"
		//and x is independent from global scope
		return function(){return x;}
	}
};


console.log(funcList[0]());
console.log(funcList[1]())
