// can be used to create behaviour that affects multiple objects


// Javascript objects dont own functions. they just have properties. and a property can be anything
// a primitive, another object or a function

// The problem

function Car(name, speed){
    this.name = name;
    this.speed = speed;

    this.accelerate = function(){       // For every car object creation using new keywords
                                        // a new copy of accelerate function object is created.
        this.speed +=5;
    }
}
// Define these methods in one single place and have them inherited
