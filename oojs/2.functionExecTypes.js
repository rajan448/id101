function foo(){
    //Named Function declaration. w/o requiring variable assignment 
    //Function name is visible within its scope and its parent scope
    
    function bar(){
        return 3;
    }

    return bar(); 
    
    function bar(){
        return 8;
    }
}
console.log(foo());

// Function expression

// It can be named or anonymous function. Name is not visible outside the body of the fuc=nction itself
var sum = function add(a, b){
    return a+b;
}

var product = function(a, b){
    return a*b;
}

(function div(a,b){
    if(b==0) return 0;
    else{
        return a/b;
    }
})();

