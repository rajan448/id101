function add(a,b){
    // console.log(this)
    return a+b;
}

// var a = new add();
// console.log(a)

var obj = {prop : "Objects property"}

obj.add = function(a, b){
    // console.log(this);
    return a+b;
}

// console.log(obj.add(2,3));

function Car(name, speed){
    this.name = name;
    this.speed = speed;

    this.accelerate = function(){       // if a function is nested within another function it
                                        // doesn't mean that their this reference will be same.
        this.speed +=5;
    }
}

function Driver(name){
    this.name = name;
}

var car = new Car("Honda City", 200);
var driver = new Driver("Mike");

driver.accelerate = car.accelerate; // Borrowing the car's property

console.log(car);
driver.accelerate.call(car)         // Using call method we can pass the object reference of this
console.log(car);

