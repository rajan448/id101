// Non constructor way of creating a car
function createCar(name, speed){
    var car = {}
    car.name = name
    car.speed = speed
    return car;
}

// Constructor way of creating an object
function Car(name, speed)
{
    this.name = name
    this.speed = speed;
}

car1 = createCar("Maruti", 100);
car2 = new Car("Honda City", 200);

console.log(car1)
console.log(car2)
